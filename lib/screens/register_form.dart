import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/authentication_bloc.dart';
import 'package:flutter_app/blocs/authentication_event.dart';
import 'package:flutter_app/blocs/register_bloc.dart';
import 'package:flutter_app/blocs/register_event.dart';
import 'package:flutter_app/blocs/register_state.dart';
import 'package:flutter_app/widgets/gradient_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';

class RegisterForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();

}

class ListItem {
  int value;
  String name;

  ListItem(this.name,this.value);

}

class _LoginFormState extends State<RegisterForm> {

  Position _currentPosition;
  String _currentAddress = "";
  String _currentCountry = "";
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _ageController = TextEditingController();
  final TextEditingController _genderController = TextEditingController();

  List<ListItem> _dropdownMenuSports = [
    ListItem("Football", 1),
    ListItem("Tennis", 2),
    ListItem("Basketball", 3)
  ];

  List<ListItem> _dropdownMenuAcademics = [
    ListItem("Mathematics", 1),
    ListItem("science", 2),
    ListItem("Basketball", 3)
  ];

  List<ListItem> _dropdownMenuHobbies = [
    ListItem("Culinary", 1),
    ListItem("Reading", 2),
    ListItem("Music", 3)
  ];

  List<DropdownMenuItem<ListItem>> _dropdownMenuItemsSports;
  ListItem _selectedItemSports;

  List<DropdownMenuItem<ListItem>> _dropdownMenuItemsAcademics;
  ListItem _selectedItemAcademics;

  List<DropdownMenuItem<ListItem>> _dropdownMenuItemsHobbies;
  ListItem _selectedItemHobbies;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  RegisterBloc _registerBloc;

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(_onEmailChange);
    _passwordController.addListener(_onPasswordChange);
    _dropdownMenuItemsSports = buildDropDownMenuItems(_dropdownMenuSports);
    _selectedItemSports = _dropdownMenuItemsSports[0].value;
    _dropdownMenuItemsAcademics = buildDropDownMenuItems(_dropdownMenuAcademics);
    _selectedItemAcademics = _dropdownMenuItemsAcademics[0].value;
    _dropdownMenuItemsHobbies = buildDropDownMenuItems(_dropdownMenuHobbies);
    _selectedItemHobbies = _dropdownMenuItemsHobbies[0].value;

  }

  List<DropdownMenuItem<ListItem>> buildDropDownMenuItems(List listItems) {
    List<DropdownMenuItem<ListItem>> items = List();
    for (ListItem listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
      listener: (context, state) {
        if (state.isFailure) {
          Scaffold.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Register Failure'),
                    Icon(Icons.error),
                  ],
                ),
                backgroundColor: Color(0xffffae88),
              ),
            );
        }

        if (state.isSubmitting) {
          Scaffold.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(
              SnackBar(
                content: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Registering...'),
                    CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                    )
                  ],
                ),
                backgroundColor: Color(0xffffae88),
              ),
            );
        }

        if (state.isSuccess) {
          BlocProvider.of<AuthenticationBloc>(context).add(
            AuthenticationLoggedIn(),
          );
          Navigator.pop(context);
        }
      },
      child: BlocBuilder<RegisterBloc, RegisterState>(
        builder: (context, state) {
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Form(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    controller: _emailController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.email),
                      labelText: "Email",
                    ),
                    keyboardType: TextInputType.emailAddress,
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isEmailValid ? 'Invalid Email' : null;
                    },
                  ),
                  TextFormField(
                    controller: _passwordController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.lock),
                      labelText: "Password",
                    ),
                    obscureText: true,
                    autovalidate: true,
                    autocorrect: false,
                    validator: (_) {
                      return !state.isPasswordValid ? 'Invalid Password' : null;
                    },
                  ),
                  TextFormField(
                    controller: _nameController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.person),
                      labelText: "Name",
                    ),
                  ),
                  TextFormField(
                    controller: _ageController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.calendar_today),
                      labelText: "Age",
                    ),
                    autovalidate: true,
                    autocorrect: false,
                  ),
                  TextFormField(
                    controller: _genderController,
                    decoration: InputDecoration(
                      icon: Icon(Icons.person),
                      labelText: "Gender",
                    ),
                    autovalidate: true,
                    autocorrect: false,
                  ),
                  DropdownButton(
                      hint: Text("Select your favorite sport"),
                      items: _dropdownMenuItemsSports,
                      isExpanded: true ,
                      icon: Icon(Icons.sports_handball_outlined),
                      value: _selectedItemSports,
                      onChanged: (value) {
                        setState(() {
                          _selectedItemSports = value;
                        });
                      }),
                  DropdownButton(
                      hint: Text("Select your favorite academic activity"),
                      items: _dropdownMenuItemsAcademics,
                      isExpanded: true,
                      icon: Icon(Icons.menu_book_rounded),
                      value: _selectedItemAcademics,
                      onChanged: (value) {
                        setState(() {
                          _selectedItemAcademics = value;
                        });
                      }),
                  DropdownButton(
                      icon: Icon(Icons.music_note),
                      hint: Text("Select your hobbie"),
                      items: _dropdownMenuItemsHobbies,
                      isExpanded: true,
                      value: _selectedItemHobbies,
                      onChanged: (value) {
                        setState(() {
                          _selectedItemHobbies = value;
                        });
                      }),
                  if(_currentPosition != null)
                    Text(_currentAddress),
                  FlatButton(
                      onPressed: (){
                        _getCurrentLocation();
                      },
                      child: Text("Get Location")
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  GradientButton(
                    width: 150,
                    height: 45,
                    onPressed: () {
                      if (isButtonEnabled(state)) {
                        _onFormSubmitted();
                      }
                    },
                    text: Text(
                      'Register',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    icon: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _onEmailChange() {
    _registerBloc.add(RegisterEmailChanged(email: _emailController.text));
  }

  void _onPasswordChange() {
    _registerBloc.add(RegisterPasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    List<String> preferencias = new List();
    preferencias.add(_selectedItemSports.name);
    preferencias.add(_selectedItemAcademics.name);
    preferencias.add(_selectedItemHobbies.name);
    _registerBloc.add(RegisterSubmitted(
        email: _emailController.text,
        password: _passwordController.text,
        age: int.parse(_ageController.text),
        gender: _genderController.text,
        name: _nameController.text,
        location: _currentCountry,
        preferences: preferencias
    ));
  }
  _getCurrentLocation() async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });
      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _currentAddress = "${place.locality}, ${place.postalCode}, ${place.country}";
        _currentCountry = "${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }
}
