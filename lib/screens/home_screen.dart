import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/FirebaseServices.dart';
import 'package:flutter_app/blocs/authentication_bloc.dart';
import 'package:flutter_app/blocs/authentication_event.dart';
import 'package:flutter_app/blocs/plans_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeScreen extends StatelessWidget {
  final User user;
  const HomeScreen({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              BlocProvider.of<AuthenticationBloc>(context)
                  .add(AuthenticationLoggedOut());
            },
          )
        ],
      ),
      body:BodyWidget(
        user: this.user,
      ),
    );
  }
}

class BodyWidget extends StatelessWidget {

  final User user;
  BodyWidget({this.user});

  List<PlanState> planes = [];
  List<String> actividades = [
    "https://www.youtube.com/watch?v=JWTyO8npkOQ",
    "https://www.youtube.com/watch?v=vY49eNceLBo",
    "https://www.youtube.com/watch?v=0mcwOaV1gWY"
  ];
  @override
  Widget build(BuildContext context) {
    _onPressed2();
    return ListView(
      padding: EdgeInsets.all(10.0),
      children: actividades
          .map((data) => ListTile(
        leading: Icon(Icons.video_collection_rounded),
        title: Text(data),
        onTap: (){
          _launchURL(data);
        },
      ))
          .toList(),
    );
  }

  List _onPressed() {
    FirebaseFirestore.instance.collection("Plans").get().then((querySnapshot) {
      querySnapshot.docs.forEach((result) {
        PlanState plan = PlanState.fromJson(result.data());
        planes.add(plan);
      });
    });
    print("onPressed" + planes.length.toString());
    return planes;
  }

  _onPressed2() async {
    FirebaseServices preferencias = new FirebaseServices(user: this.user);
    preferencias.getVideos();
  }
  List getActivities()
  {
    return planes[0].activities;
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}


