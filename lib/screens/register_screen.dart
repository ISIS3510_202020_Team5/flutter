import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/register_bloc.dart';
import 'package:flutter_app/repositories/user_repository.dart';
import 'package:flutter_app/screens/register_form.dart';
import 'package:flutter_app/widgets/curved_widget.dart';
import 'package:flutter_bloc/flutter_bloc.dart';


class RegisterScreen extends StatelessWidget {
  final UserRepository _userRepository;

  const RegisterScreen({Key key, UserRepository userRepository})
      : _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: Color(0xff6a515e),
        ),
      ),
      body: BlocProvider<RegisterBloc>(
        create: (context) => RegisterBloc(userRepository: _userRepository),
        child: Container(
          height: double.infinity,
          color: Colors.white,
          child: SingleChildScrollView(
            child: Stack(
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 100, left: 50),
                    width: double.infinity,
                    height: 220,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange[900], Colors.orange[900].withOpacity(0.4)],
                      ),
                    ),
                    child: Text(
                      'FUNKIDS        Register',
                      style: TextStyle(
                        fontSize: 40,
                        color: Color(0xff6a515e),
                        fontFamily: 'RobotoMono',
                      ),
                    ),
                  ),
                Container(
                  margin: const EdgeInsets.only(top: 230),
                  child: RegisterForm(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}