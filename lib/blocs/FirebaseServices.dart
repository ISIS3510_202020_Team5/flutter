import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/plans_state.dart';

class FirebaseServices {
  FirebaseFirestore _fireStoreDataBase = FirebaseFirestore.instance;
  User user;

  FirebaseServices({this.user});
  //recieve the data

  Future<List<String>> getPreferences() async {
    List algo;
    await _fireStoreDataBase.collection("Users").doc(user.email).get().then((value) => algo = value.data()['Preferences']);
    var result = algo.cast<String>().toList();
    return result;
  }

  Future<List<String>> getVideos() async {
    List<String> preferences = await getPreferences();
    String location = await getLocation();
    List videos = new List();
    //for (String preference in preferences) {
      var algo;
      if (location == 'Colombia') {
        await _fireStoreDataBase.collection("VideosEs").doc(preferences[0])
            .get()
            .then((value) => algo = value.data()['Video']);
        List<String> video = algo.cast<String>().toList();
        for (String v in video) {
          videos.add(v);
        }

        /*await _fireStoreDataBase.collection("VideosEs").doc(preferences[1])
            .get()
            .then((value) => algo = value.data()['Video']);
        video = algo.cast<String>().toList();
        for (String v in video) {
          videos.add(v);
        }

        await _fireStoreDataBase.collection("VideosEs").doc(preferences[2])
            .get()
            .then((value) => algo = value.data()['Video']);
        video = algo.cast<String>().toList();
        for (String v in video) {
          videos.add(v);
        }*/

      }
      else {
        await _fireStoreDataBase.collection("VideosEs").doc(preferences[0])
            .get()
            .then((value) => algo = value.data()['Video']);
        List<String> video = algo.cast<String>().toList();
        for (String v in video) {
          videos.add(v);
        }
      }

      /*List<String> video = algo.cast<String>().toList();
      for (String v in video) {
        videos.add(v);
      }*/
    print(videos.length);
    return algo;
  }

  Future<String> getLocation() async{
    var algo;
    await _fireStoreDataBase.collection("Users").doc(user.email).get().then((value) => algo = value.data()['Location']);
    print(algo);
    return algo;
  }

  Future<String> getName() async{
    var algo;
    await _fireStoreDataBase.collection("Users").doc(user.email).get().then((value) => algo = value.data()['Name']);
    print(algo);
    return algo;
  }

  Future<String> getAge() async{
    var algo;
    await _fireStoreDataBase.collection("Users").doc(user.email).get().then((value) => algo = value.data()['Age']);
    print(algo);
    return algo;
  }

  Stream<List<PlanState>> getPlansList() {
    return _fireStoreDataBase.collection('Plans')
        .snapshots()
        .map((snapShot) =>
        snapShot.docs
            .map((document) => PlanState.fromJson(document.data()))
            .toList());
  }
}