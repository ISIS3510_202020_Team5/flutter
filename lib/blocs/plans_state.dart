class PlanState {
  List activities;
  String ages;
  List preferences ;
  String planName;
  List videoNames;

  PlanState({this.activities,this.ages,this.planName,this.preferences,this.videoNames});

  PlanState.fromJson(Map<String, dynamic> parsedJSON)
    : activities = parsedJSON['Actividades'],
      ages = parsedJSON['Edades'],
      preferences = parsedJSON['Gustos'],
      planName = parsedJSON['Nombre'],
      videoNames = parsedJSON['nombres'];

}