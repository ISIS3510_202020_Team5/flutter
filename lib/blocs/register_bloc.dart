import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/blocs/register_event.dart';
import 'package:flutter_app/blocs/register_state.dart';
import 'package:flutter_app/repositories/user_repository.dart';
import 'package:flutter_app/utils/Validator.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterBloc extends Bloc<RegisterEvent, RegisterState> {
  final UserRepository _userRepository;

  RegisterBloc({UserRepository userRepository})
      : _userRepository = userRepository,
        super(RegisterState.initial());

  @override
  Stream<RegisterState> mapEventToState(RegisterEvent event) async* {
    if (event is RegisterEmailChanged) {
      yield* _mapRegisterEmailChangeToState(event.email);
    } else if (event is RegisterPasswordChanged) {
      yield* _mapRegisterPasswordChangeToState(event.password);
    } else if (event is RegisterSubmitted) {
      yield* _mapRegisterSubmittedToState(
          email: event.email, password: event.password, age: event.age , gender: event.gender,location: event.location,name: event.name, preferences: event.preferences);
    }
  }

  Stream<RegisterState> _mapRegisterEmailChangeToState(String email) async* {
    yield state.update(isEmailValid: Validator.isValidEmail(email));
  }

  Stream<RegisterState> _mapRegisterPasswordChangeToState(String password) async* {
    yield state.update(isPasswordValid: Validator.isValidPassword(password));
  }

  Stream<RegisterState> _mapRegisterSubmittedToState(
      {String email, String password,int age , String gender , String location , String name, List<String> preferences}) async* {
    yield RegisterState.loading();
    try {
      await _userRepository.signUp(email, password);
      _addUser(age, email, gender, location, name, preferences);
      print(_userRepository.getUser());
      yield RegisterState.success();
    } catch (error) {
      print(error);
      yield RegisterState.failure();
    }
  }

  Future <void> _addUser(int age, String email , String gender , String location , String name, List<String> preferences)
  async {
    try{
      await Firestore.instance.collection("Users").doc(email).set(
        {
          'Age' : age,
          'Email' : email,
          'Gender' : gender,
          'Location' : location,
          'Name' : name,
          'Preferences' : FieldValue.arrayUnion(preferences),
        });
    }
    catch(e)
    {
      print(e);
    }
  }
}